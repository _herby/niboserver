# niboServer

a [Sails](http://sailsjs.org) application

install node
https://nodejs.org/en/

run `$ npm install`

cd into redis-3.0.0/ 

`$ ./src/redis-server`

`$ sails lift`

either hit 

POST http://localhost:1234/nibos with HTTP BODY: {"name" : "YourName"}

or 

GET http://localhost:1234/nibos/create?name=YouName

to create a nibo - The interface should show the created one now