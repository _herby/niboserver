/**
 * IncidentController
 *
 * @description :: Server-side logic for managing incidents
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    /** 
        POST /incidents
    */
	create: function(req, res) {
        var niboId = req.param('nibo');
        RedisService.message('istart', niboId);
        Incident.create({nibo: niboId})
            .then(function(incident) {
                return res.ok(incident);
            })
            .catch(function(err) {
                return res.serverError({error: err});
            });
        setTimeout(function() {
            RedisService.message('iend', niboId);
        }, 5000);
    },
    /** 
        POST /incidents/:id
    */
    update: function(req, res) {
        var niboId = req.param('nibo');
        RedisService.messege('iend', niboId);
    }
};

