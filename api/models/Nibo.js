/**
* Nibo.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
    name: {
        type: 'string',
        required: true
    },
    incidents: {
        collection: 'incident',
        via: 'nibo'
    },
    //without (0) or with incident (1)
    status: {
        type: 'integer',
        enum: [0, 1],
        defaultsTo: 0
    }
  }
};