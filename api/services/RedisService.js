/* 
* @Author: Stefan Wirth
* @Date:   2015-09-12 09:58:50
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2015-12-14 23:53:41
*/
var redis = require('redis')
  , Promise = require('bluebird')
  , redisConf = function() {
    if (process.env.NODE_ENV !== 'production') {
      return sails.config.redis || {};
    }
    return {host: process.env.REDIS_HOST, port: process.env.REDIS_PORT};
}(),
client = redis.createClient(redisConf.port, redisConf.host),
publisher = redis.createClient(redisConf.port, redisConf.host);

client.subscribe('istart');
client.subscribe('iend');

client.on('message', function(channel, message) {
    console.log(message);
    var niboId = message;

    if(channel === 'istart') {
        var newStatus = 1;
        Promise.all([
            Incident.create({nibo : niboId}),
            Nibo.update({id: niboId}, {status: newStatus})
        ])
        .then(function(results) {
            var incident = results[0];
            var nibos = results[1];
            Incident.publishCreate(incident);
            nibos.forEach(function(nibo) {
                Nibo.publishUpdate(nibo.id, {status: newStatus})
            });
        })
        .catch(function(err) {
            sails.log.error(err);
        });
    } else if(channel === 'iend') {
        var newStatus = 0;
        var date = new Date();
        Promise.all([
            Incident.update({nibo: niboId, isResolved: false}, {resolvedAt: date, isResolved: true}),
            Nibo.update({id: niboId}, {status: newStatus})
        ])
        .then(function(results) {
            var incidents = results[0];
            var nibos = results[1]; 
            incidents.forEach(function(incident) {
                Incident.publishUpdate(incident.id, {resolvedAt: date});
            });
            nibos.forEach(function(nibo) {
                Nibo.publishUpdate(nibo.id, {status: newStatus})
            }); 
        }).catch(function(err) {
            sails.log.error(err);
        });
    }
});

module.exports.message = function(channelName, msg) {
    publisher.publish(channelName, msg)
};

// setTimeout(function() {
//     message();
// }, 5000);
// //client part of xbee and nibo
// function message() {
//     setTimeout(function() {
//         publisher.publish('istart', "10");
//         setTimeout(function() {
//             publisher.publish('iend', "10");
//         }, 5000);
//     }, 5000);
// }